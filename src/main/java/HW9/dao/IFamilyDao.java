package HW9.dao;

import HW9.entity.*;

import java.util.List;

public interface IFamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex (int index);

    Family getFamilyById (long id);

    boolean deleteFamily (int index);

    boolean deleteFamily (Family family);

    void saveFamily (Family family);
}
