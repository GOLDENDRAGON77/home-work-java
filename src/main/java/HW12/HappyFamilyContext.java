package HW12;

import HW12.controller.FamilyController;
import HW12.dao.CollectionFamilyDao;
import HW12.dao.IFamilyDao;
import HW12.service.FamilyService;

public class HappyFamilyContext {
    public static FamilyController familyController;

    public static void init() {
        IFamilyDao familyDao = new CollectionFamilyDao();

        FamilyService familyService = new FamilyService(familyDao);
        familyController = new FamilyController(familyService);

    }
}