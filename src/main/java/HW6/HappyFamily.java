package HW6;


public class HappyFamily {
    public static void main(String[] args) {
        String[] petHabits={"кушать", "спать", "гулять"};
        Dog petty=new Dog("Тор", 2, 80, petHabits);
        System.out.println(petty);
        petty.respond();


        Man man = new Man("Вадим", "Мещеряков", 1977);
        man.setIq(80);
        Man woman = new Man("Ирина", "Горобец", 1981);
        woman.setIq(70);

        Family family = new Family (woman, man);
        family.bornChild();
        System.out.println(family.getChildren()[0].toString());
    }
}
