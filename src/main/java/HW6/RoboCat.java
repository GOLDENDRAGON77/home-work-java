package HW6;


public class RoboCat extends Pet {
    private Species species = Species.ROBO_CAT;

    RoboCat() {
        super();
    }
    RoboCat(String nickname) {
        super(nickname);
    }

    RoboCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я Искуственный интелект\n", getNickname());
    }
}
