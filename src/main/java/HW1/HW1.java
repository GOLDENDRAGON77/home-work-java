package HW1;

import java.util.Scanner;
import java.util.Random;

class GuessANumber {
    public static void main(String[] args) {

        int UnknownNumber, UserNumber, TrysCount = 0;
        String UserName;
        Scanner input = new Scanner(System.in);

        System.out.println("Guess the number (from 0 to 100)!");
        System.out.println("Let the game begin!");
        System.out.print("Enter Your name: ");

        UserName = input.next();

        Random randNumber = new Random();
        UnknownNumber= randNumber.nextInt(100) + 1;

        do {
            TrysCount++;

            System.out.print("Enter Your number: ");

            UserNumber = input.nextInt();

            if (UserNumber > UnknownNumber)
                System.out.println("Your number is too big. Please, try again.");

            else if (UserNumber < UnknownNumber)
                System.out.println("Your number is too small. Please, try again.");

            else
                System.out.println("Congratulations," + UserName );

        } while (UserNumber != UnknownNumber);

        System.out.println("Number of attempts: " + TrysCount);

    }
}