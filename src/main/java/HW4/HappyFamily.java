package HW4;

public class HappyFamily {
    public static void main(String[] args) {
        String[] petHabits={"Кушать", "Спать", "Гулять"};
        Pet dog=new Pet("Собака", "Тор", 2, 80, petHabits);
        Pet cat=new Pet("Кошка", "Мурка", 3, 80, petHabits);

        String[][] momSchedule={
                {"Понедельник", "Сериалы смотреть"},
                {"Вторник", "Сериалы смотреть"},
                {"Среда", "Сериалы смотреть"},
                {"Четверг", "Сериалы смотреть"},
                {"Пятница", "Сериалы смотреть"},
                {"Суббота", "с детьми заниматься"},
                {"Воскресенье", "Мужу уделять внимание"}
        };
        String[][] dadSchedule={
                {"Понедельник", "в DAN IT учиться"},
                {"Вторник", "в DAN IT учиться"},
                {"Среда", "в DAN IT учиться"},
                {"Четверг", "в DAN IT учиться"},
                {"Пятница", "в DAN IT учиться"},
                {"Суббота", "с друзьями в баню"},
                {"Воскресенье", "отдых после быни"}
        };
        String[][] sonSchedule={
                {"Понедельник", "в школу"},
                {"Вторник", "в школу"},
                {"Среда", "в школу"},
                {"Четверг", "в школу"},
                {"Пятница", "в школу"},
                {"Суббота", "отдыхать"},
                {"Воскресенье", "отдыхать"}
        };
        String[][] daughterSchedule={
                {"Понедельник", "в садик"},
                {"Вторник", "в садик"},
                {"Среда", "в садик"},
                {"Четверг", "в садик"},
                {"Пятница", "в садик"},
                {"Суббота", "на артакционы"},
                {"Воскресенье", "активные игры с семьей"}
        };

        Human mom=new Human("Ирина", "Горобец", 1981, 100,momSchedule);
        Human dad=new Human("Вадим", "Мещеряков", 1977, 100, dadSchedule);
        Human son=new Human("Ростислав", "Горобец", 1995,  100, sonSchedule);
        Human daughter=new Human("Мария", "Мещерякова", 2016,  100, daughterSchedule);

        Family family=new Family(mom, dad);
        family.addChild(son);
        family.addChild(daughter);
        family.setPet(dog);

        Human momTwo=new Human("Полина", "Прокофьева", 1981, 100,momSchedule);
        Human dadTwo=new Human("Почьтальен", "Печькин", 1977, 100, dadSchedule);
        Human sonTwo=new Human("Курьер", "Почьтальенов", 1995,  100, sonSchedule);
        Human daughterTwo=new Human("Алиса", "Селезнева", 2016,  100, daughterSchedule);

        Family familyTwo=new Family(momTwo, dadTwo);
        familyTwo.addChild(sonTwo);
        familyTwo.addChild(daughterTwo);
        familyTwo.setPet(cat);

        toStringFamily(dog, family);

        toStringFamily(cat, familyTwo);

    }

    private static void toStringFamily(Pet dog, Family family) {
        dog.eat();
        dog.respond();
        dog.foul();
        System.out.println(dog.toString());

//        son.greetPet();
//        son.describePet();
//        son.feedPet(false);
//        System.out.println(son.toString());

        System.out.println(family.toString());
        System.out.println(family.countFamily());
    }
}
