package HW5;

public class HappyFamily {
    public static void main(String[] args) {
        String[] petHabits={"кушать", "спать", "гулять"};
        Pet petty=new Pet(Species.DOG, "Тор", 2, 80, petHabits);


        String[][] momSchedule={
                {DayOfWeek.MONDAY.name(), "Сериалы смотреть"},
                {DayOfWeek.TUESDAY.name(), "Сериалы смотреть"},
                {DayOfWeek.WEDNESDAY.name(), "Сериалы смотреть"},
                {DayOfWeek.THURSDAY.name(), "Сериалы смотреть"},
                {DayOfWeek.FRIDAY.name(), "Сериалы смотреть"},
                {DayOfWeek.SATURDAY.name(), "с детьми заниматься"},
                {DayOfWeek.SUNDAY.name(), "Мужу уделять внимание"}
        };
        String[][] dadSchedule={
                {DayOfWeek.MONDAY.name(), "в DAN IT учиться"},
                {DayOfWeek.TUESDAY.name(), "в DAN IT учиться"},
                {DayOfWeek.WEDNESDAY.name(), "в DAN IT учиться"},
                {DayOfWeek.THURSDAY.name(), "в DAN IT учиться"},
                {DayOfWeek.FRIDAY.name(), "в DAN IT учиться"},
                {DayOfWeek.SATURDAY.name(), "с друзьями в баню"},
                {DayOfWeek.SUNDAY.name(), "отдых после быни"}
        };
        String[][] sonSchedule={
                {DayOfWeek.MONDAY.name(), "в школу"},
                {DayOfWeek.TUESDAY.name(),  "в школу"},
                {DayOfWeek.WEDNESDAY.name(), "в школу"},
                {DayOfWeek.THURSDAY.name(), "в школу"},
                {DayOfWeek.FRIDAY.name(), "в школу"},
                {DayOfWeek.SATURDAY.name(), "отдыхать"},
                {DayOfWeek.SUNDAY.name(), "отдыхать"}
        };

        String[][] daughterSchedule={
                {DayOfWeek.MONDAY.name(), "в садик"},
                {DayOfWeek.TUESDAY.name(), "в садик"},
                {DayOfWeek.WEDNESDAY.name(), "в садик"},
                {DayOfWeek.THURSDAY.name(), "в садик"},
                {DayOfWeek.FRIDAY.name(), "в садик"},
                {DayOfWeek.SATURDAY.name(), "на артакционы"},
                {DayOfWeek.SUNDAY.name(), "активные игры с семьей"}
        };

        Human mom=new Human("Ирина", "Горобец", 1981, 100, momSchedule);
        Human dad=new Human("Вадим", "Мещеряков", 1977, 100, dadSchedule);
        Human son=new Human("Ростислав", "Горобец", 1995,  100, sonSchedule);
        Human daughter=new Human("Мария", "Мещерякова", 2016,  100, daughterSchedule);


        Family family=new Family(mom, dad);
        family.addChild(son);
        family.addChild(daughter);
        family.setPet(petty);

        petty.eat();
        petty.respond();
        petty.foul();
        System.out.println(petty.toString());

        son.greetPet();
        son.describePet();
        son.feedPet(false);
        System.out.println(son.toString());

        System.out.println(family.toString());
        System.out.println(family.countFamily());

//        int i=0;
//        while(i < 100000){Human human = new Human(); i++;}
        Pet pet1 = new Pet();
        System.out.println(pet1);

        family.deleteChild(1);
    }
}
