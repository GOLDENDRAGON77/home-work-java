package HW8;

import HW7.Dog;
import HW7.Man;
import HW7.Woman;
import HW8.controller.FamilyController;
import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {
        Set<String> petHabits = new HashSet<>();
        petHabits.add("кушать");
        petHabits.add("спать");
        petHabits.add("гулять");

        Dog petty=new Dog("Тор", 2, 80, petHabits);


        Man man = new Man("Вадим", "Мещеряков", 1977);
        man.setIq(80);
        Woman woman = new Woman("Ирина", "Горобец", 1981);
        woman.setIq(70);

        Man man2 = new Man("Почтальен", "Печкин", 1980);
        man.setIq(85);
        Woman woman2 = new Woman("Алиса", "Селезнева", 1990);
        woman.setIq(92);
        Man man3 = new Man("Константин", "Рожков", 2018);
        man.setIq(35);

        FamilyController familyController = new FamilyController();

        familyController.createNewFamily(woman,man);
        familyController.createNewFamily(woman2,man2);

        System.out.println(familyController.getAllFamilies());

        familyController.bornChild(familyController.getFamilyById(1), "Игорь", "Ирина");

        familyController.adoptChild(familyController.getFamilyById(2), man3);

        familyController.addPet(0, petty);

        System.out.println(familyController.getPets(0));

        familyController.displayAllFamilies();

        System.out.println(familyController.getFamiliesBiggerThan(2));

        System.out.println(familyController.getFamiliesLessThan(5));

        System.out.println(familyController.countFamiliesWithMemberNumber(2));

        System.out.println(familyController.deleteFamilyByIndex(1));

        familyController.deleteAllChildrenOlderThen(18);

        System.out.println(familyController.count());

    }
}
